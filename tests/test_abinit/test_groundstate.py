import numpy as np
from pathlib import Path

def test_groundstate(si_groundstate):
    # Test that the ground state calculation was successful:
    properties = si_groundstate.read_results()

    assert properties.get('energy', None) is not None
    assert properties.get('forces', None) is not None
    assert properties.get('stress', None) is not None

    # This assumes the choice of pseudo-potential.
    np.testing.assert_allclose(properties['energy'], -212.99020858, atol=1e-5)

    # We will want the density file to exist: 
    expected_path = si_groundstate.directory / 'calco_DEN' # This name is hardcoded for now (From AbipyTemplate)
    assert Path.exists(expected_path), f'Expected path {expected_path} does not exist.'
