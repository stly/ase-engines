from asetest import DataFiles
from ase.build import bulk
import numpy as np
from pathlib import Path
import pytest
import os

configvar = 'ASE_ENGINES_CONFIG'

@pytest.fixture(autouse=True)
def abipy():
    return pytest.importorskip('abipy')

@pytest.fixture(scope='session')
def config():
    from configparser import ConfigParser
    try:
        configpath = os.environ[configvar]
    except KeyError:
        pytest.skip(
            reason=f'No configuration, please set ${configvar}=<file>')
    parser = ConfigParser()
    parser.read(configpath)
    return parser

@pytest.fixture(scope='session')
def profile(config):
    from ase.calculators.abinit import AbinitProfile
    exes = config['executables']
    if 'abinit' not in exes:
        pytest.skip(reason='no abinit in configuration')
    profile = AbinitProfile([exes['abinit']])
    return profile

@pytest.fixture(scope='session')
def abinit(profile):
    from engines.abinit.abinit import Abinit
    return Abinit(profile)

@pytest.fixture(scope='session')
def si_pseudo_potential_path():
    datafiles = DataFiles()
    abinit_path = datafiles.paths['abinit'][0] # The zero takes the LDA ones.
    return abinit_path / '14-Si.LDA.fhi'


@pytest.fixture(scope='session')
def si_groundstate(tmp_path_factory, si_pseudo_potential_path, abinit):
    # Ground structure calculation:
    atoms = bulk('Si')

    # uses the pseudo-potential in the tests directory.
    pseudo_potentials = [str(si_pseudo_potential_path)]

    parameters = {
            'ecut': 6,
            'tolwfr': 1e-8,
            'ngkpt': '1 1 1',
            'chksymbreak': 0
            }
    gs_directory = tmp_path_factory.mktemp('abinit_gs/')
    gs = abinit.groundstate(atoms, parameters, pseudo_potentials, gs_directory)
    return gs


@pytest.fixture(scope='session')
def si_relax(tmp_path_factory, si_pseudo_potential_path, abinit):
    # Ground structure calculation:

    atoms = bulk('Si')

    directory = tmp_path_factory.mktemp('abinit_relax/')

    # Hardcoded currently. Will need to decide on something smarter.. 
    pseudo_potentials = [str(si_pseudo_potential_path)]
    
    # Parameters for the relaxation:
    atoms.positions[0] += 0.25
    relax_parameters = dict(ecut=8, tolwfr=1e-5, ntime=1)
    relax_directory = directory / 'relax'
    rc = abinit.relaxation(atoms, relax_parameters,
                        pseudo_potentials, relax_directory)
    return rc


@pytest.fixture(scope='session')
def si_bandpath(si_groundstate): # Kind of a hack to have access to the bandpath in both the fixture and the test. 
    atoms = si_groundstate.atoms
    bandpath = atoms.cell.bandpath('GXWK', density=1)
    return bandpath

@pytest.fixture(scope='session')
def si_bandstructure(tmp_path_factory, si_groundstate, si_bandpath, abinit):
    # Band structure calculation:
    bandstructure_parameters = {}
    bs_directory = tmp_path_factory.mktemp('abinit_bs/')
    bs = abinit.bandstructure(si_groundstate, si_bandpath, bandstructure_parameters, bs_directory)
    return bs

@pytest.fixture(scope='session')
def si_dos(tmp_path_factory, si_groundstate, abinit):
    directory = tmp_path_factory.mktemp('abinit_bandgap/')
    bandgap_parameters = dict(tolwfr=1e-8)
    kpts = [1, 1, 1]
    dos = abinit.dos(si_groundstate, kpts, bandgap_parameters, directory)
    return dos 

@pytest.fixture(scope='session')
def si_bandgap(tmp_path_factory, si_groundstate, abinit):
    directory = tmp_path_factory.mktemp('abinit_bandgap/')
    bandgap_parameters = dict(tolwfr=1e-8)
    bg = abinit.bandgap(si_groundstate, bandgap_parameters, directory)
    return bg

@pytest.fixture(scope='session')
def si_screening(tmp_path_factory, si_bandgap, abinit):
    directory = tmp_path_factory.mktemp('abinit_screening/')
    screening_parameters = dict()
    sc = abinit.screening_ppmode(si_bandgap, screening_parameters, directory)
    return sc

@pytest.fixture(scope='session')
def si_gw(tmp_path_factory, si_screening, si_bandgap, abinit):
    directory = tmp_path_factory.mktemp('abinit_gw/')
    gw_parameters = dict()
    gwo = abinit.gw(si_bandgap, si_screening, gw_parameters, directory)
    gwo.read_results()
    return gwo


@pytest.fixture(scope='session')
def aln_pseudo_potential():
    datafiles = DataFiles()
    abinit_path = datafiles.paths['abinit'][0] # The zero takes the LDA ones. 
    pseudos = [str(abinit_path / '13-Al.LDA.fhi'),
               str(abinit_path / '07-N.LDA.fhi')]
    return pseudos


@pytest.fixture(scope='session')
def aln_groundstate(tmp_path_factory, aln_pseudo_potential, abinit):
    # Ground structure calculation:

    atoms = bulk('AlN', 'wurtzite', 3.13)

    # Hardcoded currently. Will need to decide on something smarter..? 
    pseudo_potentials = aln_pseudo_potential

    parameters = {'ecut': 8,
                  'tolvrs': 1e-12,
                  'ngkpt': '1 1 1',
                  'chksymbreak': 0,
                  'diemac': 9}
    gs_directory = tmp_path_factory.mktemp('abinit_gs/')
    gs = abinit.groundstate(atoms, parameters, pseudo_potentials, gs_directory)
    return gs


@pytest.fixture(scope='session')
def aln_dielectric(tmp_path_factory, aln_groundstate, abinit):
    directory = tmp_path_factory.mktemp('abinit_dielectric/')
    ddk_parameters = dict(tolerance=1e-15)
    dde_parameters = dict(tolerance=1e-8)
    dr = abinit.dielectric_response(aln_groundstate, ddk_parameters,
                                 dde_parameters, directory)
    
    return dr
